provider "aws" {
  region = "us-east-1"
}

resource "tls_private_key" "tmp" {
  algorithm   = "RSA"
}

resource "aws_key_pair" "user-ssh-key" {
  key_name   = "my-efs-mount-key"
  public_key = tls_private_key.tmp.public_key_openssh
}

resource "aws_instance" "instance" {
    ami = "ami-0947d2ba12ee1ff75"
    instance_type = "t2.micro"
    key_name = aws_key_pair.user-ssh-key.key_name
    security_groups = [aws_security_group.connection.name]
  
    tags = {
      Name = "my_ec2"
    }

}

resource "aws_instance" "second_instance" {
    ami = "ami-0947d2ba12ee1ff75"
    instance_type = "t2.micro"
    key_name = aws_key_pair.user-ssh-key.key_name
    security_groups = [aws_security_group.connection.name]
  
    tags = {
      Name = "my_ec2_second"
    }

}

resource "aws_security_group" "connection" {
  name = "connect_ec2"
  description = "connect for ec2 instance"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }

  ingress {
    from_port = 2049
    to_port = 2049
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }

   egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_efs_file_system" "efs" {
  creation_token = "my-efs"
  tags = {  
    Name = "MyProduct"
  }
}

resource "aws_efs_mount_target" "mount" {
  file_system_id = aws_efs_file_system.efs.id
  subnet_id      = aws_instance.instance.subnet_id
  security_groups = [aws_security_group.connection.id]
}


resource "null_resource" "mounting_efs" {
  depends_on = [aws_efs_mount_target.mount]
  provisioner "remote-exec" {
      inline = [
        "sleep 10s",
        "sudo mkdir -p /mnt/efs",
        "sudo mount -t nfs -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport ${aws_efs_file_system.efs.dns_name}:/  /mnt/efs",
        "sudo touch /mnt/efs/hello.txt"
      ]

      connection {
        host        = aws_instance.instance.public_ip
        type        = "ssh"
        user        = "ec2-user"
        private_key = tls_private_key.tmp.private_key_pem
      }
    }
}

resource "null_resource" "second_mounting_efs" {
  depends_on = [null_resource.mounting_efs]
  provisioner "remote-exec" {
      inline = [
        "sleep 10s",
        "sudo mkdir -p /mnt/efs",
        "sudo mount -t nfs -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport ${aws_efs_file_system.efs.dns_name}:/  /mnt/efs",
      ]

      connection {
        host        = aws_instance.second_instance.public_ip
        type        = "ssh"
        user        = "ec2-user"
        private_key = tls_private_key.tmp.private_key_pem
      }
    }
}


